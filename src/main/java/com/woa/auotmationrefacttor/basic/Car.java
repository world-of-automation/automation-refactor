package com.woa.auotmationrefacttor.basic;

public class Car {


    private int year;
    private String make;

    //same name as the class name
    // no void/return type
    public Car() {

    }

    public Car(int year) {
        this.year = year;
    }

    public Car(int year, String make) {
        Laptop laptop = new Laptop();
        laptop.printBrand();
        this.year = year;
        this.make = make;
    }
    // any methods we will have a void or return type and the name would be whatever you wan t


    public void printtCar() {
        System.out.println(year);
        System.out.println(make);
    }


}
