package com.woa.auotmationrefacttor.basic;

public class LaptopExec {

    public static void main(String[] args) {

        // class refVar = new constructor
        Laptop laptop = new Laptop();
        laptop.printBrand();

        boolean container0 = Laptop.getTouchbar();
        System.out.println(container0);

        System.out.println(laptop.model);


        boolean container = Laptop.getTouchbar();
        System.out.println(container);

    }
}
