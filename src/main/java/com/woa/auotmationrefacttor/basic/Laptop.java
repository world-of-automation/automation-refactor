package com.woa.auotmationrefacttor.basic;

public class Laptop {
    private static final int year = 2010;

    // access specifiers
    // public, private, protected, default
    // https://www.geeksforgeeks.org/access-modifiers-java/

    // variables & data types
    // all the variables or methods are static or non static
    // if you don't declare thats non static as default

    // static --> static , non-static
    // static if in diff class --> classname.method/variablesName
    // non static --> non-static (unless obj invoked)
    private static final boolean touchbar = true;

    static {
        System.out.println("Printing from static block");
    }

    private final String brand = "apple";
    public String model = "macbookPro";

    // default constructor  is always there
    // constructor --> to create the obj
    public Laptop() {

    }

    //double;
    //float;

    // methods
    // main method is always static
    public static void main(String[] args) {
        System.out.println(year);
        System.out.println(touchbar);
        getTouchbar();
    }

    // return type method
    public static boolean getTouchbar() {
        return touchbar;
    }

    // void method
    public void printBrand() {
        System.out.println(brand);
        //System.out.println(touchbar);
    }

    public void laptopTest() {
        printBrand();
        getTouchbar();
    }


}


