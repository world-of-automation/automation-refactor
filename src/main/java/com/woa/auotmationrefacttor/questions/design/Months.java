package com.woa.auotmationrefacttor.questions.design;

public enum Months {
    January, February, March, April, May, June, July, August, September, October, November, December
}
