package com.woa.auotmationrefacttor.oop.polymorphism;

public class Calculator {


    public static void add(int a, int b) {
        System.out.println(a + b);
    }


    public static void add(int a, int b, int c) {
        System.out.println(a + b + c);
    }

    public static void main(String[] args) {
        add(1, 2);

        add(1, 2, 3);
    }


}
