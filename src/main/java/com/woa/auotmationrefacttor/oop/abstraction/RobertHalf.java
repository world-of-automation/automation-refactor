package com.woa.auotmationrefacttor.oop.abstraction;

public class RobertHalf extends Recruiting2 implements BusinessConcept, BusinessConcept2 {

    public void name() {
        System.out.println("RobertHalf");
    }

    public void location() {
        System.out.println("Manhattan");
    }

    public int getProfit() {
        return 100000;
    }

    public boolean ipo() {
        return true;
    }

    public void accountent() {
        System.out.println("not needed now");
    }

    // i - i
    // a - e
    // extend -- before implementing
}
