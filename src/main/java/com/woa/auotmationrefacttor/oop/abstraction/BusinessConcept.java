package com.woa.auotmationrefacttor.oop.abstraction;

public interface BusinessConcept {

    String name = "data";

    void name();

    void location();

    int getProfit();

    boolean ipo();

}
