package com.woa.auotmationrefacttor.oop.abstraction;

public class Execution {

    public static void main(String[] args) {

        RobertHalf robertHalf = new RobertHalf();
        robertHalf.accountent();
        robertHalf.name();
        robertHalf.hireRecruiter();
    }
}
