package com.woa.auotmationrefacttor.oop.encapsulation;


public class Exec {

    public static void main(String[] args) {

        Student student = new Student();
        student.setId(10);
        student.setName("S");
        student.setPassed(true);

        System.out.println(student.getId());
        System.out.println(student.getName());
        System.out.println(student.isPassed());


        System.out.println(student.toString());


    }
}
