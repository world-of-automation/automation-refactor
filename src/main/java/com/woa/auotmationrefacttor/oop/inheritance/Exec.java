package com.woa.auotmationrefacttor.oop.inheritance;

public class Exec {
    public static void main(String[] args) {
        C c = new C();

        c.methodFromC();
        c.methodFromB();
        c.methodFromA();

    }
}
