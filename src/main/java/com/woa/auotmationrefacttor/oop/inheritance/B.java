package com.woa.auotmationrefacttor.oop.inheritance;

public class B extends A {
    public void methodFromB() {
        System.out.println("B CLASS");
    }
}
