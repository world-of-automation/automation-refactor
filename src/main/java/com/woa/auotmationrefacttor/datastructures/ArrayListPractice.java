package com.woa.auotmationrefacttor.datastructures;

import java.util.ArrayList;

public class ArrayListPractice {

    public static void main(String[] args) {


        ArrayList<String> country = new ArrayList();
        country.add("USA");
        country.add("BD");
        country.add("AUS");

        System.out.println(country.size());
        System.out.println(country);

        System.out.println(country.contains("BS"));

        System.out.println(country.get(1));
        System.out.println(country.get(2));

        country.remove(2);

        System.out.println(country);

        // country.clear();
        System.out.println(country);


        for (int i = 0; i < country.size(); i++) {
            System.out.println(country.get(i));
        }


        ArrayList<String> state = new ArrayList();
        state.add("NY");
        state.add("NJ");
        state.add("DC");

        ArrayList<ArrayList<String>> arrayListObjectt = new ArrayList<ArrayList<String>>();

        arrayListObjectt.add(country);
        arrayListObjectt.add(state);

        System.out.println(arrayListObjectt);


    }


}
