package com.woa.auotmationrefacttor.datastructures;

import java.util.Random;

public class ArrayPractice {


    // write the method which will take an array
    // and return a random text/number/any data types (as per the array) everytime

    public static Object getArandomData(Object[] arrayObject) {
        Random random = new Random();
        int randomNumFromArray = random.nextInt(arrayObject.length);
        return arrayObject[randomNumFromArray];
    }


    // write a method which will take a String array and a String on param
    // and if that array has the string in any of its indexes, your method will replace that index's value to found
    // and if that array doesn't have the string in any of its indexes, you method will replace that index value to not-found
    // and finally return the updated array


    public static String[] getNewArray(String[] arrayObject, String key) {
        for (int i = 0; i < arrayObject.length; i++) {
            if (arrayObject[i].equalsIgnoreCase(key)) {
                arrayObject[i] = "found";
            } else {
                arrayObject[i] = "not-found";
            }
        }
        return arrayObject;
    }

    // write a method which will take a String array which may have numbers inside the String (example "1","2","fake","who"etc)
    // and return a int array with all the numbers from the String array

    public static int[] getFormattedArray(String[] numbersAndData) {
        int container = 0;
        int[] newArray = new int[numbersAndData.length];
        for (int i = 0; i < numbersAndData.length; i++) {
            try {
                container = Integer.valueOf(numbersAndData[i]);
                newArray[i] = container;
            } catch (NumberFormatException ee) {
            }
        }
        return newArray;
    }

    public static void main(String[] args) {
        String[] numbersAndData = {"2020", "NY", "2021", "NJ", "2010"};
        int[] cleanedArray = getFormattedArray(numbersAndData);
        for (int i = 0; i < cleanedArray.length; i++) {
            System.out.println(cleanedArray[i]);
        }


        String name = "XYZ,ABC";
        String[] names = {"XYZ", "ABC", "BCD", "GHD", "YYU", "HHD"};

        Object randomFromMethod = getArandomData(names);

        System.out.println("randomFromMethod : " + randomFromMethod);

        System.out.println(name);
        System.out.println(names[0]);

        System.out.println(names[1]);

        String[] newNames = names.clone();
        System.out.println(newNames.length);


        int[] years = new int[7];

       /* years[0]=2020;
        years[5]=2025;
        years[7]=2027;


        System.out.println(years[7]);
        System.out.println(years[8]);*/

        Random random = new Random();
        for (int i = 0; i < years.length; i++) {
            years[i] = random.nextInt(100);
        }

        for (int i = 0; i < years.length; i++) {
            System.out.println(years[i]);
        }

        String[] state = {"NY", "NJ", "NY", "VA", "DC", "NY"};

        String[] newArray = getNewArray(state, "NY");


        for (int i = 0; i < newArray.length; i++) {
            System.out.println(newArray[i]);
        }
    }





}
