package com.woa.auotmationrefacttor.datastructures;

import java.util.HashSet;
import java.util.Iterator;

public class HashSetPractice {

    public static void main(String[] args) {
        HashSet<String> setOfData = new HashSet<String>();
        setOfData.add("NY");
        setOfData.add("NY");
        setOfData.add("NJ");
        System.out.println(setOfData);

        System.out.println(setOfData.size());


        Iterator iterator = setOfData.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
