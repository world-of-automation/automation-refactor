package com.woa.auotmationrefacttor.datastructures;

import java.util.HashMap;
import java.util.HashSet;

public class ArrayPractice2 {

    // run a loop on data 1
    // for individual data of data1, run another loop on data2
    // if data1's first index matches any of the index of data2
    // store into a hashset
    // finally print the hashset

    public static void main(String[] args) {

        String[] data1 = {"NY", "NJ", "DC", "VA"};
        String[] data2 = {"AZ", "VA", "RI", "AK", "NY", "VA", "NY", "NY"};

        HashMap<String, Integer> map = new HashMap<String, Integer>();
        HashSet<String> setOfData = new HashSet<String>();


        for (int i = 0; i < data1.length; i++) {

            for (int j = 0; j < data2.length; j++) {

                if (data1[i].equalsIgnoreCase(data2[j])) {

                    setOfData.add(data2[j]);
                    System.out.println("line ran");

                    if (map.containsKey(data2[j])) {
                        map.put(data2[j], map.get(data2[j]) + 1);
                    } else {
                        map.put(data2[j], 1);
                    }
                }
            }
        }

        System.out.println(map);
        System.out.println(setOfData);


    }
}
