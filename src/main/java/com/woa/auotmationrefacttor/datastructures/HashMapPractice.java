package com.woa.auotmationrefacttor.datastructures;

import java.util.HashMap;

public class HashMapPractice {


    public static void main(String[] args) {
        HashMap<String, String> mapOfCountry = new HashMap<String, String>();

        mapOfCountry.put("NY", "11374");
        mapOfCountry.put("DC", "03278");
        mapOfCountry.put("NJ", "34632");
        mapOfCountry.put("VA", "23425");

        System.out.println(mapOfCountry.get("DC"));

        mapOfCountry.remove("VA");

        System.out.println(mapOfCountry);

        System.out.println(mapOfCountry.containsKey("AZ"));

        System.out.println(mapOfCountry.size());


    }

}
